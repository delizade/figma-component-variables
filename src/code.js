const groupBy = require('lodash/groupBy');

figma.showUI(__html__);

setComponent();

figma.on('selectionchange', () => {
	setComponent();
});

figma.ui.resize(300, 200);

figma.ui.onmessage = event => {
		if (event.type === 'cancel') figma.closePlugin();
		if (event.type === 'update-name') updateName(event);
		if (event.type === 'update-input') updateInput(event);
		if (event.type === 'update-selector') updateSelector(event);
		if (event.type === 'update-condition') updateCondition(event);
		if (event.type === 'update-height') updateWindowHeight(event);
};

async function setComponent() {
	const selected = figma.currentPage.selection[0];
	if (!selected || ['INSTANCE'].indexOf(selected.type) === -1) {
		figma.ui.postMessage({ type: 'unset-inputs', count: 0 });
		return;
	}

	if (figma.currentPage.selection.length === 1) {
		const info = getInfo(selected);
		const inputs = await getInputs(selected);
		const selectors = getSelectors(selected);
		const conditions = getConditions(selected);
		figma.ui.postMessage({ type: 'set-inputs', info, inputs, selectors, conditions });
	} else {
		figma.ui.postMessage({ type: 'unset-inputs', count: figma.currentPage.selection.length });
	}
}

function getInfo(selected) {
	const info = {};
	info.name = selected.name,
	info.masterComponent = {
		name: selected.masterComponent.name,
		id: selected.masterComponent.id,
	};
	return info;
}

function getInputs(selected) {
	return new Promise((resolve) => {
		const textNodes = selected.findAll(n => n.type === 'TEXT' && n.name.startsWith('{')).filter(n => !n.hasMissingFont);
		const textItems = textNodes.map(n => ({ id: n.id, label: n.name.replace('{', '').replace('}', ''), value: n.characters }));

		const fonts = [];
		textNodes.forEach(n => {
			n.locked = true;
			figma.loadFontAsync({ family: n.fontName.family, style: n.fontName.style });
		});
		Promise.all(fonts).then(() => {
			const inputs = groupBy(textItems, 'label')
			resolve(Object.keys(inputs).length ? inputs : null);
		});
	});
}

function getSelectors(selected) {
	const selectorNodes = selected.findAll(n => n.name.startsWith('['));
	const reset = [];
	const selectors = {};
	selectorNodes.forEach(n => {
		const labels = n.name.replace('[', '').replace(']', '').split('.');
		if (!selectors[labels[0]]) selectors[labels[0]] = {};
		if (selectors[labels[0]][labels[1]] && selectors[labels[0]][labels[1]] !== n.visible) reset.push(labels[0]);
		else selectors[labels[0]][labels[1]] = n.visible;
	});

	// reset if can bug
	reset.forEach(group => {
		Object.keys(selectors[group]).forEach((k, i) => {
			selected
				.findAll(n => n.name === `[${group}/${k}]`)
				.forEach(n => n.visible = i === 0);
			selectors[group][k] = i === 0;
		});
	});

	return Object.keys(selectors).length ? selectors : null;
}

function getConditions(selected) {
	const conditionNodes = selected.findAll(n => n.name.startsWith('('));
	const reset = [];
	const conditions = {};

	conditionNodes.forEach(n => {
		const cond = n.name.replace('(', '').replace(')', '');
		const label = cond.replace('!', '');
		const value = cond.indexOf('!') === -1;
		const bool = n.visible ? value : !value;
		if (conditions[label] === undefined) conditions[label] = bool;
		if (conditions[label] !== bool) reset.push(label);
		else conditions[label] = bool;
	});

	reset.forEach(c => {
		selected
			.findAll(n => (n.name === `(${c})` || n.name === `(!${c})`))
			.forEach(n => n.visible = n.name.indexOf('!') > -1);
		conditions[c] = false;
	});

	return Object.keys(conditions).length ? conditions : null;
}

function updateName(event) {
	const node = figma.currentPage.selection[0];
	node.name = event.name;
	setComponent();
}

function updateInput(event) {
	const nodes = figma.currentPage.selection[0].findAll(n => n.type === 'TEXT' && n.name === `{${event.newField.label}}`);
	nodes.forEach((n) => {
		n.characters = event.newField.value;
	});
	setComponent();
}

function updateSelector(event) {
	const selected = figma.currentPage.selection[0];
	const nodes = selected.findAll(n => {
		const labels = n.name.replace('[', '').replace(']', '').split('.');
		const isGroup = labels[0] === event.group;
		if (isGroup) return n;
	})

	nodes.forEach(n => {
		const labels = n.name.replace('[', '').replace(']', '').split('.');
		const isVisible = labels[1] === event.selected;
		n.visible = isVisible;
	});
	setComponent();
}

function updateCondition(event) {
	const selected = figma.currentPage.selection[0];
	const labels = [`(${event.condition})`, `(!${event.condition})`];
	const all = selected
		.findAll(n => labels.indexOf(n.name) > -1)
		.forEach(n => {
			n.visible = event.visible.toString() === (n.name.indexOf('!') === -1).toString();
		});
	setComponent();
}

function updateWindowHeight(event) {
	const limit = figma.viewport.bounds.height - 40;
	const height = event.height + 10 > limit ? limit : event.height + 10;
	figma.ui.resize(300, Math.round(height));
}
