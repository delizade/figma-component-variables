module.exports = {
	onCreate(input) {
		this.state = {
			checked: input.checked,
		};
	},

	onInput(input) {
		if (input.checked !== this.state.checked) this.setState({ checked: input.checked });
	},

	emitChange() {
		this.setState({ checked: !this.state.checked });
		this.emit('change', this.state.checked);
	},
};
