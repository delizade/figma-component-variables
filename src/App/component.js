module.exports = {
	onCreate() {
		this.state = {
			unset: true,
			info: null,
			inputs: null,
			selectors: null,
			conditions: null,
			ready: false,
		};
	},

	onUpdate() {
		this.updateHeight();
	},

	onMount() {
		onmessage = (response) => {
			const event = response.data.pluginMessage;
			if (event && event.type) {
				switch (event.type) {
					case 'set-inputs': {
						this.setState({
							info: event.info,
							inputs: event.inputs,
							selectors: event.selectors,
							conditions: event.conditions,
							unset: 1,
							ready: true,
						});
					}
						break;
					case 'unset-inputs': {
						this.setState({
							info: null,
							inputs: null,
							selectors: null,
							conditions: null,
							unset: event.count,
							ready: true,
						});
					}
						break;
				}
			}
		};
	},

	changeInstanceName(ev) {
		parent.postMessage({ pluginMessage: { type: 'update-name',  name: ev.target.value } }, '*');
	},

	changeInput(field, ev) {
		const newField = { label: field };
		newField.value = ev.target.value;
		parent.postMessage({ pluginMessage: { type: 'update-input',  newField } }, '*');
	},

	changeSelector(group, ev) {
		parent.postMessage({ pluginMessage: { type: 'update-selector',  group, selected: ev.target.value } }, '*');
	},

	changeCondition(condition, visible) {
		parent.postMessage({ pluginMessage: { type: 'update-condition',  condition, visible } }, '*');
	},

	updateHeight() {
		const { height } = this.getEl().getBoundingClientRect();
		parent.postMessage({ pluginMessage: { type: 'update-height',  height } }, '*');
	},

	closeWindow() {
		parent.postMessage({ pluginMessage: { type: 'cancel' } }, '*');
	}
};
