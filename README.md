# Figma Component Variables
A plugin that helps customize Figma components in an orderly and automatic manner.
Create and edit preset variables within your component without the hassle of going through groups and autolayouts.


## HOW TO USE

### TEXT VARIABLES
*Assign a variable name to a text layer and change the text content through the panel.*

Name the text layer wrapping it in curly brackets. The name inside will be assigned to the variable.

```E.g.: {label} will create a variable label.```

### SELECTOR VARIABLES
*Create grouped options which only one is shown at a time.*

Wrap the layer name in brackets, the first name should be the group name and the second the variable name. Separate the names with a `.`.

When selected, all layers will be hidden, expect the chosen one.

```E.g.: [Color.Blue], [Color.Green] creates a group Color in which you can choose between Blue and Green colors.```

### CONDITION VARIABLES
*Show or hide a layer based on a condition.*

Name the layer wrapping it in parenthesis. This will create a toggle for the variable name which you can show/hide on the panel. 

```E.g.: (disabled)```

You can create an inverse condition by naming the layer starting with a `!`.

```E.g.: (!disabled)```


## NOTES
- The variables should be created on the master component, keeping in mind not to rename the layers on the instance.
- The configuration panel works only with instances.
- You can create multiple layers with the same name to get the same behaviour on them. 